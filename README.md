# WIP: SoCKit OpenCPI OSP

This is the source discribution of the OpenCPI SoCKit System Support Project (OSP)

OpenCPI is Open Source Software, licensed with the LGPL3. See LICENSE file  

This process will build kernel version 4.9.78

Additional tools and setup files:
  available online (fill in url for dl)
  Can be done by downloading these files  
cyclonev-17.1.0.590.qdz
QuartusSetup-17.1.1.593-linux.run  OR
QuartusLiteSetup-17.1.0.590-linux.run
SoCEDSSetup-17.1.0.590-linux.run
cyclonev-17.1.0.590.qdz

Alternative method

  +++ start commands +++

  github.com/CTSRD-CHERI/quartus-install
    https://github.com/CTSRD-CHERI/quartus-install.git

  cd quartus-install
    run quartus-install.py   ## need aria2
    quartus-install.py version installDir platform 
    ./quartus-install.py 17.1lite ${INSTALL_DIR}/intelFPGA/17.1 c5 eds

  #replace INSTALL_DIR about with desired destination directory

  +++ end command list +++


libpng12 required by Quartus tools
If configuring a ubuntu 18.04 or greater environ you will need to add libpng12, default version is >=libpng16.
You can build or download the .deb file.

To build, use this shell script, with destination directory as argument.

  +++ start snippet +++

  #!/bin/sh

  TARGET=$1
  git clone https://git.code.sf.net/p/libpng/code libpng-code
  cd libpng-code
  git checkout libpng12
  ./configure
  make
  cp -v .libs/*.a $TARGET
  cp -v .libs/*.so* $TARGET

  +++ end snippet +++

To install via package manager, deb.
  First download libpng12-0_1.2.54-1ubuntu1.1_amd64.deb (64bit architecture)
  open/install with system installed package manager

Quartus tools:
Install per default locations.  If you install in an alternative location/path, you
will need to set correct paths in ${OCPI_HOME_DIR}/opencpi/user-env.sh

To manually build kernel, headers and kconfig, at command prompt:

ARCH=arm CROSS_COMPILE=~/intelFPGA/17.1/embedded/ds-5/sw/gcc/bin/arm-linux-gnueabihf- make socfpga_defconfig
ARCH=arm CROSS_COMPILE=~/intelFPGA/17.1/embedded/ds-5/sw/gcc/bin/arm-linux-gnueabihf- make zImage

to manually clean:
ARCH=arm CROSS_COMPILE=~/intelFPGA/17.1/embedded/ds-5/sw/gcc/bin/arm-linux-gnueabihf- make cleanall

To manually test 'createLinuxKernelHeaders.sh':
~/opencpi/projects/osps/ocpi.osp.sockit_cv/rcc/platforms/altera17_1/scripts/createLinuxKernelHeaders.sh \
17_1 ACDS17.1_REL_GSRD_UPDATE1_PR ~/intelFPGA/git


### RCC Platform Dependencies (remove me and add to OpenCPI 

Download and place prebuilt SD card image into /opt/intelFPGA/IntelReleases
https://s3.amazonaws.com/rocketboards/releases/2016.10/sockit-ghrd/bin/sockit-gsrd-16.0-sdcard.img.tar.gz 

Established (cloned) Altera linux kernel tree where the Intel FPGA tools are installed tools, e.g. /opt/intelFPGA/git 
