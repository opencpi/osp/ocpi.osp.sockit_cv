#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# See the "usage" message below
# "make" will run in parallel unless you set MAKE_PARALLEL to blank or null string (but defined)
export MAKE_PARALLEL=${MAKE_PARALLEL--j}
set -e

if test "$1" = "" -o "$1" = "--help" -o "$1" = "-help" -o "$2" = ""; then
  cat <<EOF
The purpose of this script is to create a "kernel headers" package with the necessary artifacts
for an OpenCPI Altera Linux "release" based on a specific Altera release.
It assumes:
- a git repo for Altera linux kernel and u-boot has been established (cloned) in the git/ subdirectory 
  of where the Intel FPGA tools are installed, e.g. /opt/intelFPGA/git 
- an Altera installation for OpenCPI exists in the environment
- the OCPI_CDK_DIR environment variable points to an OpenCPI installation

It does these things:
- Checks out the previously downloaded git source repo with the tag for this release
  - More infomation about what tag to use can be found by running showAlteraLinuxTags.sh or
      going to the following link: https://rocketboards.org/foswiki/Documentation/GSRDTagging
- Creates a sparse "kernel-headers" subset of the built linux source tree.
- Captures several other files from the build process in the resulting release directory

The result of this script is a release directory with everything that is needed to:
- Build the OpenCPI linux kernel driver (this completing the framework build for altera)
- Create a bootable SD card (after running createLinuxRoosFS.sh).

The name of the resulting release directory is: opencpi-altera-release-<release-name>
After this step, the git repo directory can be removed to save space.

Usage is: createLinuxKernelHeaders.sh <release-name> <repo-tag> <repo-dir> <out-dir>
e.g.: ./createLinuxKernelHeaders.sh 17_0 ACDS17.1_REL_GSRD_UPDATE1_PR git
EOF
  exit 1
fi
# Enable altera variables
source $OCPI_CDK_DIR/scripts/util.sh
setVarsFromMake $OCPI_CDK_DIR/include/hdl/altera.mk ShellIseVars=1 $verbose

if test \
  "$OcpiAlteraProgrammerDir" = "" -o \
  ! -d "$OcpiAlteraProgrammerDir" ; then \
  echo Error: the OpenCPI build environment for Altera is not set up.
  exit 1
fi

GIT_DIR_NAME=linux-socfpga

OCPI_CROSS_BUILD_BIN_DIR=$OcpiAlteraProgrammerDir/embedded/ds-5/sw/gcc/bin/
OCPI_CROSS_HOST=arm-linux-gnueabihf
CROSS_COMPILE=$OCPI_CROSS_BUILD_BIN_DIR/${OCPI_CROSS_HOST}-
rel=$1
tag=$2
case $3 in (/*) gdir=$3 ;; (*) gdir=`pwd`/$3;; esac
case $4 in (/*) odir=$4 ;; (*) odir=`pwd`/$4;; esac 
echo $odir
if test ! -d $gdir/$GIT_DIR_NAME; then
  echo The source directory $3/$GIT_DIR_NAME does not exist. Run getAlteraLinuxSources.sh\?
  exit 1
fi
# Protect against sym links for the git subdir for case sensitivity
RELDIR=$odir
if test ! -L $RELDIR; then
  mkdir -p $RELDIR
fi
if test -e ../release ; then
  unlink ../release
fi
echo Using the tag '"'$tag'"' for the Altera linux kernel source repository.
cd $gdir/$GIT_DIR_NAME
git reset --hard `git symbolic-ref --short HEAD`
git clean -ffdx
echo Checking out the Altera Linux kernel using the repository label '"'$tag'"'.
git checkout -f tags/$tag
echo ============================================================================================
echo Configuring the Altera linux kernel using their default configuration for socfpga....
export CROSS_COMPILE
export ARCH=arm
echo RELDIR=$RELDIR
echo PWD=`pwd`
patch -p1 < $RELDIR/rcc/platforms/altera17_1/scripts/patches/localVersion.patch
make socfpga_defconfig
echo ============================================================================================
echo Building the Altera linux kernel to create the kernel-headers tree.
# To build a kernel that we would use, we would do:
make zImage  # don't know why this would be pounded out... back in
patch -p1 < $RELDIR/../../scripts/patches/cyclone5_sockit_led.patch
make socfpga_cyclone5_sockit.dtb
ocpi_kernel_release=$(< include/config/kernel.release)
echo ============================================================================================
cd $RELDIR
echo Capturing the built Linux uImage file and the device trees in release directory: $(basename $RELDIR)
# add check for existance before mkdir
[ ! -e dts ] && mkdir dts
[ ! -e lib ] && mkdir lib
cp $gdir/$GIT_DIR_NAME/arch/arm/boot/dts/socfpga_cyclone5*.dt* dts
cp $gdir/$GIT_DIR_NAME/arch/arm/boot/zImage .
cp -P $OCPI_CROSS_BUILD_BIN_DIR/../$OCPI_CROSS_HOST/lib/libstdc++.so* lib
echo ============================================================================================
echo Preparing the kernel-headers tree based on the built kernel.
rm -r -f kernel-headers-$tag kernel-headers
[ ! -e kernel-headers ] && mkdir kernel-headers
# copy that avoids errors when caseinsensitive file systems are used (MacOS...)
#cp -R ../git/$GIT_DIR_NAME/{Makefile,Module.symvers,include,scripts,arch} kernel-headers
(cd $gdir/$GIT_DIR_NAME;
  for f in Makefile Module.symvers include scripts arch/arm .config; do
    find $f -type d -exec mkdir -p $RELDIR/kernel-headers/{} \;
    find $f -type f -exec sh -c \
     "if test -e $RELDIR/kernel-headers/{}; then
        echo File {} has a case sensitive duplicate which will be overwritten.
        rm -f $RELDIR/kernel-headers/{}
      fi
      cp {} $RELDIR/kernel-headers/{}" \;
  done

  for i in $(find -name 'Kconfig*'); do
    mkdir -p $RELDIR/kernel-headers/$(dirname $i)
    cp $i $RELDIR/kernel-headers/$(dirname $i)
  done
)

rm -r -f kernel-headers/arch/arm/boot
#find kernel-headers -name "*.[csSo]" -exec rm {} \;
rm kernel-headers/scripts/{basic,mod}/.gitignore
# Record the kernel release AND the repo tag used.
echo $ocpi_kernel_release > kernel-headers/ocpi-release
echo ============================================================================================
echo The kernel-headers directory/package has been created in $(basename $RELDIR)
echo It is now ready for building the OpenCPI linux kernel driver for cyclone5
echo ============================================================================================
echo Removing *.cmd
find kernel-headers -name '*.cmd' -exec rm {} \;
echo Removing *.tmp*
find kernel-headers -name '*.tmp*' -exec rm {} \;
echo Removing x86_64 binaries
find kernel-headers -type f -not -iname '*.h' -not -iname '*.c' -not -iname '*.S' -not -iname 'kconfig' -not -path "*kernel-headers/scripts/*"| xargs file | grep ELF | cut -f1 -d: | xargs -tr -n1 rm
mkdir -p $RELDIR/kernel-headers/tools/include
cp -R $gdir/linux-socfpga/tools/include/tools $RELDIR/kernel-headers/tools/include

echo Removing unused large headers
rm -rf kernel-headers/include/linux/{mfd,platform_data}
