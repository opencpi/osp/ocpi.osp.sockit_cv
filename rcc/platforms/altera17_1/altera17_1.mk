# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

##########################################################################################
# This file defines the altera17_1 software platform.
# It sets platform variables as necessary to override the defaults in the file:
#   include/platform-defaults.mk file.
# See that file for a description of valid variables and their defaults.
include $(OCPI_CDK_DIR)/include/hdl/altera.mk
AlteraToolDir:=$(OcpiAlteraProgrammerDir)/embedded/ds-5/sw/gcc/bin
ifeq ($(wildcard $(AlteraToolDir)),)
#TODO fix error message and for loop
  $(error When setting up to build for altera17_1 for $(OCPI_TARGET_PLATFORM), cannot find $(AlteraToolDir). Perhaps quartus and or embeded was not installed\
          when Altera tools were installed? The non-default Altera environment settings were: \
          $(foreach v,$(filter OCPI_ALTERA%,$(.VARIABLES)), $v=$($v)))
endif
OcpiCrossCompile=$(AlteraToolDir)/arm-linux-gnueabihf-
OcpiCFlags+=-mfpu=neon-fp16 -mfloat-abi=hard -march=armv7-a -mtune=cortex-a9
OcpiCXXFlags+=-mfpu=neon-fp16 -mfloat-abi=hard -march=armv7-a -mtune=cortex-a9
OcpiStaticProgramFlags=-rdynamic
OcpiKernelDir=kernel-headers
OcpiPlatformOs=linux
OcpiPlatformOsVersion=a17_1
OcpiPlatformArch=arm
#OcpiPlatformPrerequisites=mdev:altera17_1
